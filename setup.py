from distutils.core import setup

from setuptools import find_packages

setup(
    name="WGCommonLib",
    version="0.3.1",
    author="Oleksandr Zelentsov",
    author_email="oleksandrzelentsov@gmail.com",
    packages=find_packages(),
    scripts=[],
    url="https://gitlab.com/oleksandr.zelentsov/commonlib",
    license="LICENSE.txt",
    description="",
    long_description=open("README.rst").read(),
    install_requires=["beautifulsoup4"],
)
