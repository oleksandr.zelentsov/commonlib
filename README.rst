===========
Wgrah's Common Library
===========

This package provides a selection of functions I use a lot.
Typical usage often looks like this::

    #!/usr/bin/env python3.6

    from commonlib import function_composition as fc
    from functools import partial as p

    money_format = fc(p(str.format, '{:.2f}'), p(round, ndigits=2))
