import hashlib
import math
from contextlib import contextmanager
from typing import Optional

from .collections_utils import random_hash_stream
from .logic import logical_equivalence


def check_func(func, result, fail_msg, equal=True, *args, **kwargs):
    """
    Check if result of calling function with given args and kwargs is equal (or not equal) to the expected result.

    :param Any result: result of the function
    :param bool equal: should ``result`` be equal to result of the function
    :param callable func: tested function
    :param args: will be passed to ``func``
    :param kwargs: will be passed to ``func``
    :return:
    """
    assert logical_equivalence(func(*args, **kwargs) == result, equal), fail_msg


@contextmanager
def hide_key(
    d: dict,
    source_key: str,
    destination_key: Optional[str] = None,
    pop: Optional[bool] = True,
    setdefault: Optional[bool] = False,
):
    """
    Put a value from ``d`` identified by ``source_key`` away and add it when the context manager finishes.

    - if ``destination_key`` is specified, copy value to the ``destination_key``.

      - if ``pop`` is False, do not remove original key-value pair,
        this parameter matters only if ``destination_key`` is specified

      - if ``setdefault` is True, the existing value in ``destination_key`` will not be overwritten,
        this parameter matters only if ``destination_key`` is specified
    """
    if destination_key is None:
        destination_key = source_key

    if pop:
        get_method = d.pop
    else:
        get_method = d.get

    saved_value = get_method(source_key)
    yield saved_value
    if setdefault:
        d.setdefault(destination_key, saved_value)
    else:
        d[destination_key] = saved_value


def generate_password(length: int = 8, hash_func=hashlib.md5):
    """Generate random password of any specified length"""
    size_of_digest_in_hex_digits = hash_func().digest_size / 2
    hashes_to_generate = math.ceil(length / size_of_digest_in_hex_digits)
    hashes = [
        hash_
        for _, hash_ in zip(range(hashes_to_generate), random_hash_stream(hash_func))
    ]
    joined_hashes = "".join(hashes)
    return joined_hashes[:length]
