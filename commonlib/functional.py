import os
from contextlib import contextmanager
from functools import reduce


def function_composition(*functions):
    """
    Reversely compose functions.

    ``mystery = function_composition(lambda x: x + 2, lambda x: x * x, lambda x: x - 2)  # ((x - 2) * (x - 2)) + 2``

    ``mystery(3)  # (((3 - 2) * (3 - 2)) + 2) = 3``

    ``mystery(4)  # (((4 - 2) * (4 - 2)) + 2) = 6``

    :param callable functions: functions to compose
    :return: composition
    :rtype: callable
    """
    if len(functions) == 0:
        raise ValueError("cannot compose 0 functions")
    elif len(functions) == 1:
        return functions[0]
    elif len(functions) == 2:
        return lambda arg: functions[0](functions[1](arg))
    else:
        return reduce(function_composition, functions)


@contextmanager
def redirect_descriptor(
    file_descriptor_to_redirect: int = 2,
    redirect_to: str = os.devnull,
    oflag: int = os.O_WRONLY,
):
    """
    Redirect one descriptor's input to a file.

    By default "silences" all errors.
    """
    new_file = os.open(redirect_to, oflag)
    old_stderr = os.dup(file_descriptor_to_redirect)
    os.fdopen(file_descriptor_to_redirect).flush()
    os.dup2(new_file, file_descriptor_to_redirect)
    os.close(new_file)
    try:
        yield
    finally:
        os.dup2(old_stderr, file_descriptor_to_redirect)
        os.close(old_stderr)
