from typing import Iterable, List

from commonlib.constants import CSV_IMPORT_FIELD_NAME_SYNONYMS


def find_appropriate_name_for_field(field_name: str):
    """
    Find a corresponding DB field name for a given human-readable field name from CSV.

    :param str field_name: the given verbose string
    :return: a first found string
    :rtype: str
    """
    for (
        original_field_name,
        possible_field_names,
    ) in CSV_IMPORT_FIELD_NAME_SYNONYMS.items():
        if field_name.strip().lower() in map(str.lower, possible_field_names):
            return original_field_name

    return None


def attach_appropriate_names_for_fields(
    objects: Iterable[dict],
    strip_old_fields: bool = True,
    skip_unknown_fields: bool = False,
) -> List[dict]:
    """
    If strip_old_fields is True, just change the key names of the existing KVPs.
    If skip_unknown_fields is True, the fields that don't have an appropriate name will be skipped.
    """
    result_objects = []
    for particular_object in objects:
        field_names = list(particular_object.keys())
        for key_name in field_names:
            field_name = find_appropriate_name_for_field(key_name)
            if skip_unknown_fields and field_name is None:
                continue
            elif not skip_unknown_fields and field_name is None:
                field_name = key_name

            if strip_old_fields:
                value = particular_object.pop(key_name)
            else:
                value = particular_object.get(key_name)

            particular_object[field_name] = value

        result_objects.append(particular_object)

    return result_objects
