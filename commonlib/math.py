def convert_scale(x, in_min, in_max, out_min=0, out_max=1):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

