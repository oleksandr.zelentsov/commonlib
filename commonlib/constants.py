from typing import TypeVar

CSV_IMPORT_FIELD_NAME_SYNONYMS = {
    "total_price": [
        "paid",
        "всего",
        "total",
        "total price",
        "заплатил",
        "заплачено",
        "всего заплатил",
    ],
    "who_paid": ["хто платив", "кто платил", "кому винні", "who_paid", "who paid"],
    "email": [
        "email",
        "Email",
        "EMail",
        "Email Address",
        "E-mail",
        "E-mail Address",
        "E-mail 1",
        "E-mail 1 - Value",
    ],
    "first_name": [
        "first_name",
        "firstname",
        "FirstName",
        "firstName",
        "first name",
        "First Name",
        "Given Name",
        "Given name",
        "given Name",
        "given name",
    ],
    "last_name": [
        "last_name",
        "lastname",
        "LastName",
        "lastName",
        "last name",
        "Last Name",
        "Family Name",
        "Family name",
        "family Name",
        "family name",
    ],
    "middle_name": [
        "middle_name",
        "middlename",
        "MiddleName",
        "middleName",
        "middle name",
        "Middle Name",
    ],
    "phone_number": [
        "phone",
        "Phone",
        "phone_number",
        "phone number",
        "Phone Number",
        "telephone",
        "Telephone",
        "Primary Phone",
        "primary phone",
        "primary_phone",
        "cellphone",
        "cell phone",
        "Cell Phone",
        "cell_phone",
        "Mobile Phone",
        "mobile phone",
        "mobile_phone",
    ],
}
K = TypeVar("K")
V = TypeVar("V")
T = TypeVar("T")

LOGICAL_NON_NEGATION_TEMPLATE = "{}"
LOGICAL_NEGATION_TEMPLATE = "{}\u0305"
# LOGICAL_DISJUNCTION_SYMBOL = '\u2228'
LOGICAL_DISJUNCTION_SYMBOL = "+"
# LOGICAL_CONJUNCTION_SYMBOL = '\u2227'
LOGICAL_CONJUNCTION_SYMBOL = ""

LOGICAL_VARIABLE_TEMPLATE = "{letter}{sign}"
