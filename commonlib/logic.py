def base10_to_base2(number: int) -> str:  # todo docs
    return bin(number)[2:]


def logical_equivalence(a, b):
    return not (bool(a) ^ bool(b))


def logical_implication(a, b):
    return (not bool(a)) or bool(b)
