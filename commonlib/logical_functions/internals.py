from typing import NamedTuple, Tuple


class TruthTableEntry(NamedTuple):
    arguments: Tuple[bool]
    result: bool

    @property
    def argument_count(self):
        return len(self.arguments)


TruthTable = Tuple[TruthTableEntry]
