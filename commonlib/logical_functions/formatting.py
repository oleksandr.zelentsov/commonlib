from abc import abstractmethod
from itertools import islice, count
from string import ascii_uppercase
from typing import List, Union, Callable

from .func import LogicalFunction
from commonlib.constants import (
    LOGICAL_CONJUNCTION_SYMBOL,
    LOGICAL_DISJUNCTION_SYMBOL,
    LOGICAL_NEGATION_TEMPLATE,
    LOGICAL_NON_NEGATION_TEMPLATE,
)


class BaseLogicalFunctionFormatter:
    """
    A class to format logical functions differently.
    """

    def __init__(self, constant_0, constant_1):
        self._constant_0 = constant_0
        self._constant_1 = constant_1

    def string_representation(self, f: LogicalFunction):
        """Get a formatted string."""
        if all(f.table_results):
            return self._constant_1
        elif not any(f.table_results):
            return self._constant_0

        arg_nums_equal_to_result, inverse_arg_nums_equal_to_result = self.get_arguments_that_influence_result(
            f
        )
        if len(arg_nums_equal_to_result) == 1:
            return self.get_non_negated_value(
                self.argument_names(f)[arg_nums_equal_to_result[-1]]
            )
        elif len(inverse_arg_nums_equal_to_result) == 1:
            return self.get_negated_value(
                self.argument_names(f)[inverse_arg_nums_equal_to_result[-1]]
            )

        dnf = self.disjunctive_normal_form(f)
        knf = self.conjunctive_normal_form(f)
        if len(dnf) < len(knf):
            return dnf
        elif len(dnf) > len(knf):
            return knf
        else:
            return dnf

    @staticmethod
    def get_arguments_that_influence_result(f):
        col_nums_true = []
        col_nums_false = []
        for i in range(f.argument_count):
            args_column_true = [arg[i] for arg in f.table_arguments]
            args_column_false = [not arg[i] for arg in f.table_arguments]
            if args_column_true == f.table_results:
                col_nums_true.append(i)
            elif args_column_false == f.table_results:
                col_nums_false.append(i)

        return col_nums_true, col_nums_false

    @abstractmethod
    def disjunctive_normal_form(self, f):
        pass

    @abstractmethod
    def conjunctive_normal_form(self, f):
        pass

    @abstractmethod
    def get_negated_value(self, val):
        pass

    @abstractmethod
    def get_non_negated_value(self, val):
        pass

    @abstractmethod
    def argument_names(self, f):
        pass


class AlphabetLettersAsVariablesMixin:
    def __init__(self, letter_alphabet=ascii_uppercase):
        self._letter_alphabet = letter_alphabet

    def argument_names(self, f: LogicalFunction) -> list:
        return list(islice(self.variable_names(), f.argument_count))

    def variable_names(self):
        for letter in self._letter_alphabet:
            yield letter

        for arg_number in count():
            for letter in self._letter_alphabet:
                yield "{}{}".format(letter, arg_number)


class StringLogicalFunctionFormatter(
    AlphabetLettersAsVariablesMixin, BaseLogicalFunctionFormatter
):
    def __init__(
        self,
        *,
        conjunction_str=LOGICAL_CONJUNCTION_SYMBOL,
        disjunction_str=LOGICAL_DISJUNCTION_SYMBOL,
        negation_template=LOGICAL_NEGATION_TEMPLATE,
        non_negation_template=LOGICAL_NON_NEGATION_TEMPLATE,
        letter_alphabet=ascii_uppercase,
        constant_0="0",
        constant_1="1"
    ):
        BaseLogicalFunctionFormatter.__init__(
            self, constant_0=constant_0, constant_1=constant_1
        )
        AlphabetLettersAsVariablesMixin.__init__(self, letter_alphabet=letter_alphabet)
        self._conjunction_str = conjunction_str
        self._disjunction_str = disjunction_str
        self._negation_template = negation_template
        self._non_negation_template = non_negation_template

    def disjunctive_normal_form(self, f):
        members = []
        arg_names = self.argument_names(f)
        for result_idx, result_value in filter(
            lambda ir: ir[1], enumerate(f.table_results)
        ):
            result_arguments = f.table_arguments[result_idx]
            members.append(
                self._conjunction_str.join(
                    self._dnf_generate_arguments(arg_names, result_arguments)
                )
            )
        return self._build_disjunctive_normal_form(members)

    def conjunctive_normal_form(self, f):
        members = []
        arg_names = self.argument_names(f)
        for result_idx, result_value in filter(
            lambda ir: not ir[1], enumerate(f.table_results)
        ):
            result_arguments = f.table_arguments[result_idx]
            members.append(
                "({})".format(
                    self._build_disjunctive_normal_form(
                        self._cnf_generate_arguments(arg_names, result_arguments)
                    )
                )
            )

        result = self._build_conjunctive_normal_form(members)
        result = self._cnf_strip_redundant_parentheses(result)
        return result

    def get_non_negated_value(self, val):
        return self._negation_template.format(val)

    def get_negated_value(self, val):
        return self._non_negation_template.format(val)

    def _build_disjunctive_normal_form(self, res):
        return self._disjunction_str.join(res)

    def _build_conjunctive_normal_form(self, res):
        result = self._conjunction_str.join(res)
        return result

    @staticmethod
    def _cnf_strip_redundant_parentheses(result):
        if result.startswith("(") and result.endswith(")") and "(" not in result[1:-1]:
            result = result[1:-1]
        return result

    def _cnf_format_result_arguments(self, arg_names, result_arguments):
        return

    def _dnf_generate_arguments(self, arg_names, result_arguments):
        return [
            (
                self._negation_template if not is_true else self._non_negation_template
            ).format(arg_name)
            for is_true, arg_name in zip(result_arguments, arg_names)
        ]

    def _cnf_generate_arguments(self, arg_names, result_arguments):
        return [
            (
                self._negation_template if is_true else self._non_negation_template
            ).format(arg_name)
            for is_true, arg_name in zip(result_arguments, arg_names)
        ]


def get_html_function(f: LogicalFunction):
    from bs4 import NavigableString

    return NavigableString(str(f))


# class HTMLStringLogicalFunctionFormatter(AlphabetLettersAsVariablesMixin, BaseLogicalFunctionFormatter):
#
#     def __init__(self, *, letter_alphabet=ascii_uppercase, soup: BeautifulSoup = None):
#         BaseLogicalFunctionFormatter.__init__(
#             self,
#             constant_0=NavigableString('0'),
#             constant_1=NavigableString('1'),
#         )
#         AlphabetLettersAsVariablesMixin.__init__(
#             self,
#             letter_alphabet=letter_alphabet,
#         )
#         if soup is None:
#             soup = BeautifulSoup('', 'http.parser')
#
#         self.__soup = soup
#         self._conjunction_str = LOGICAL_CONJUNCTION_SYMBOL
#         self._disjunction_str = LOGICAL_DISJUNCTION_SYMBOL
#
#     def get_negated_value(self, val):
#         neg = self.__soup.new_tag('span', style='text-decoration: overline;')
#         neg.string = val
#         return neg
#
#     def get_non_negated_value(self, val):
#         return NavigableString(val)
#
#     def disjunctive_normal_form(self, f):
#         result = NavigableString('')
#         arg_names = self.argument_names(f)
#         numerated_results = list(enumerate(f.table_results))
#         true_results = list(filter(lambda ir: ir[1], numerated_results))
#         for result_idx, result_value in true_results:
#             result_arguments = f.table_arguments[result_idx]
#             genned_args = self._dnf_generate_arguments(arg_names, result_arguments)
#
#             for i, genned_arg in enumerate(genned_args):
#                 result.append(genned_arg)
#                 if i < len(genned_args) - 1:
#                     result.append(self._conjunction_str)
#
#             is_it_last = (result_idx == true_results[-1][0])
#             if not is_it_last:
#                 result.append(self._disjunction_str)
#
#         return result
#
#     def conjunctive_normal_form(self, f):
#         result = NavigableString('')
#         arg_names = self.argument_names(f)
#         numerated_results = enumerate(f.table_results)
#         false_results = filter(lambda ir: not ir[1], numerated_results)
#         for result_idx, result_value in false_results:
#             result_arguments = f.table_arguments[result_idx]
#             generated_arguments = self._cnf_generate_arguments(arg_names, result_arguments)
#             built_form = self._build_disjunctive_normal_form(generated_arguments)
#             result.append(
#                 '({})'.format(
#                     built_form
#                 )
#             )
#
#         result = self._build_conjunctive_normal_form(members)
#         result = self._cnf_strip_redundant_parentheses(result)
#         return result
#
#     def variable_names(self):
#         val = super().variable_names()
#         return map(NavigableString, val)
#
#     def _dnf_generate_arguments(self, arg_names, result_arguments):
#         return [
#             (self.get_negated_value if not is_true else self.get_non_negated_value)(arg_name)
#             for is_true, arg_name in zip(result_arguments, arg_names)
#         ]
#
#     def _cnf_generate_arguments(self, arg_names, result_arguments):
#         return [
#             (self.get_negated_value if is_true else self.get_non_negated_value)(arg_name)
#             for is_true, arg_name in zip(result_arguments, arg_names)
#         ]


def get_html_action_table(
    x: List[LogicalFunction], y: List[Union[int, LogicalFunction]], func: Callable
):
    from bs4 import BeautifulSoup
    x = list(x)
    y = list(y)
    results = {a: {b: func(a, b) for b in y} for a in x}

    soup = BeautifulSoup("", "html5lib")

    style = soup.new_tag("style")
    style.string = """
    th, tr, td { border: 1px solid black; font-family: monospace; }
    """
    soup.head.append(style)
    soup.head.append(soup.new_tag("meta", attrs={"charset": "utf-8"}))

    table = soup.new_tag("table")

    header = soup.new_tag("tr")

    operator_desc = soup.new_tag("th")
    operator_desc.string = func.__name__
    header.append(operator_desc)

    for val in y:
        column_header = soup.new_tag("th")
        html_value = get_html_function(val)
        column_header.append(html_value)
        header.append(column_header)

    table.append(header)

    for function_ in x:
        func_row = soup.new_tag("tr")
        func_name = soup.new_tag("td")
        func_name.string = str(function_)
        func_row.append(func_name)
        for value in y:
            result_element = soup.new_tag("td")
            val = results[function_][value]
            html_value = get_html_function(val)
            result_element.string = html_value
            func_row.append(result_element)

        table.append(func_row)

    soup.body.append(table)

    return soup.prettify()
