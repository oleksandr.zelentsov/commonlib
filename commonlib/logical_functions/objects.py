from .func import LogicalFunction

AND = LogicalFunction.from_truth_table_values("0001")
OR = LogicalFunction.from_truth_table_values("0111")
NEGATION = NOT = LogicalFunction.from_truth_table_values("10")
EQUIVALENCE = LogicalFunction.from_truth_table_values("1001")

# multiplication_table = [(x, y, x*y) for x in LogicalFunction.from_range(16) for y in LogicalFunction.from_range(16)]
