import operator
import cmath
import contextlib
import itertools
import math
from typing import Tuple, Union, List, Generator
from hashlib import sha3_512

from commonlib import function_composition
from commonlib.logic import base10_to_base2
from .internals import TruthTableEntry, TruthTable


class LogicalFunction:
    def __init__(self, table: TruthTable, *, formatter=None):
        self.__table = table
        self.__str_repr = None
        if formatter is None:
            from commonlib.logical_functions.formatting import (
                StringLogicalFunctionFormatter,
            )

            formatter = StringLogicalFunctionFormatter()

        self.__formatter = formatter

    def __call__(self, *args):
        if not len(args) == self.argument_count:
            raise ValueError("wrong type or number of arguments passed")

        for table_args, result in self.__table:
            if args == table_args:
                return result

        raise TypeError()

    @classmethod
    @contextlib.contextmanager
    def int_math(cls, *fs):
        if len(fs) == 0:
            raise ValueError("one or more functions expected")
        if len(fs) == 1:
            yield fs[0].function_space_number
        else:
            yield tuple([f.function_space_number for f in fs])

    @property
    def argument_count(self) -> int:
        """How many arguments does this function have."""
        return self.__table[-1].argument_count

    @property
    def function_space_number(self) -> int:
        """What number corresponds to this function."""
        var = "".join(map(function_composition(str, int), self.table_results))[::-1]
        return int(var, base=2)

    @property
    def table_results(self) -> List[bool]:
        return [x.result for x in self.__table]

    @property
    def table_arguments(self) -> List[Tuple[bool]]:
        return [x.arguments for x in self.__table]

    @property
    def table(self) -> list:
        return list(self.__table)

    @property
    def formatter(self):
        return self.__formatter

    @formatter.setter
    def formatter(self, new_formatter):
        self.__formatter = new_formatter
        self.__regenerate_string_representation()

    def __str__(self):
        if self.__str_repr is None:
            self.__regenerate_string_representation()

        return self.__str_repr

    def __repr__(self):
        return "LogicalFunction({0})".format(
            base10_to_base2(self.function_space_number).zfill(self.argument_count)
        )

    def __add__(self, other: Union["LogicalFunction", int]):
        if isinstance(other, int):
            with self.int_math(self) as n:
                int_result = n + other
                width = self.argument_count
        else:
            with self.int_math(self, other) as (first, second):
                int_result = first + second
                width = max([self.argument_count, other.argument_count])

        return self.from_function_space_number(int_result, width)

    def __mul__(self, other: Union["LogicalFunction", int]):
        if isinstance(other, int):
            with self.int_math(self) as n:
                int_result = n * other
                width = self.argument_count
        else:
            with self.int_math(self, other) as (first, second):
                int_result = first * second
                width = max([self.argument_count, other.argument_count])

        return self.from_function_space_number(int_result, width)

    def __invert__(self):
        inverted_results = ["1" if int(x) == 0 else "0" for x in self.table_results]
        inverted_results = "".join(inverted_results)
        return self.from_truth_table_values(inverted_results)

    def __pow__(self, power: int, modulo=None):
        if not (isinstance(power, int) and power > 0):
            raise ValueError("power should be a positive integer")

        powers = list(itertools.accumulate([self] * power, operator.mul))
        product_result = powers[-1]
        if modulo is None:
            return product_result
        else:
            raise NotImplementedError

    def __eq__(self, other):
        return set(self.__table) == set(other.__table)

    def __hash__(self):
        val = self.__class__.__name__ + str(hash(self.__table))
        return int(sha3_512(val.encode()).hexdigest(), base=16)

    @classmethod
    def from_truth_table_values(cls, number: str) -> "LogicalFunction":
        table_row_count = len(number)
        args = cls.__generate_arguments_for_logical_function(bit_count=table_row_count)
        table = cls.__generate_logical_table(args, number)
        return cls(table)

    @classmethod
    def from_function_space_number(cls, number: int, width: int = None):
        base2 = base10_to_base2(number)
        if width is None:
            width = len(base2)

        return cls.from_truth_table_values(base2.zfill(width)[::-1])

    @classmethod
    def from_range(cls, *range_args) -> Generator["LogicalFunction", None, None]:
        range_ = range(*range_args)
        r_range = reversed(range_)
        last = next(r_range)
        arg_count = len(base10_to_base2(last))
        yield from (cls.from_function_space_number(x, arg_count) for x in range_)

    @classmethod
    def all_with_n_arguments(cls, n: int) -> Generator["LogicalFunction", None, None]:
        yield from cls.from_range(2 ** 2 ** n)

    def __regenerate_string_representation(self):
        self.__str_repr = self.__formatter.string_representation(self)

    @classmethod
    def __generate_logical_table(cls, args, result_bits):
        table = tuple(
            [
                TruthTableEntry(arguments, bool(int(bit)))
                for arguments, bit in zip(args, result_bits)
            ]
        )
        return table

    @classmethod
    def __generate_arguments_for_logical_function(
        cls, *, bit_count: int = None, arg_count: int = None
    ) -> List[Tuple[bool]]:
        if bit_count is None and arg_count is None:
            raise ValueError("one of the arguments should not be None")
        elif arg_count is None:
            arg_count = int(math.ceil(cmath.log(bit_count, 2).real))
        elif bit_count is None:
            bit_count = 2 ** arg_count

        args = [
            tuple([bool(int(x)) for x in base10_to_base2(i).zfill(arg_count)])
            for i in range(bit_count)
        ]
        return args


LF = lf = LogicalFunction
