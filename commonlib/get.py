from typing import Callable, Union, Any


def _dispatch_item(item):
    def _f(obj):
        if isinstance(obj, dict):
            return obj[item]

        attr = getattr(obj, item)
        if callable(attr):
            return attr()

        return attr

    _f.__name__ = item
    return Get(_f)


class _MetaGet(type):
    def __getattr__(self, item):
        return _dispatch_item(item)


class Get(metaclass=_MetaGet):
    """
    Use to generate getters.

    >>> user = dict(first_name="John", last_name="Williams")  # OR an actual User object with the fields.
    >>> Get.first_name(user)  # "John"
    >>> Get.last_name(user)  # "Williams"
    >>> getter = Get.first_name.lower + " " + Get.last_name.lower
    >>> getter(user)  # "John Williams"
    """
    def __init__(self, f: Callable):
        self._f = f

    def __getattr__(self, item):
        def _inner_f(obj):
            v = self._f(obj)
            return _dispatch_item(item)(v)

        _inner_f.__name__ = f"{self._f.__name__}.{item}"
        return Get(_inner_f)

    def __call__(self, *args, **kwargs):
        return self._f(*args, **kwargs)

    def __add__(self, other: Union[Any, "Get"]):
        if isinstance(other, Get):
            def new_f(obj):
                return self._f(obj) + other._f(obj)

            new_f.__name__ = f"{self._f.__name__} + {other._f.__name__}"
        else:
            def new_f(obj):
                return self._f(obj) + other

            new_f.__name__ = f"{self._f.__name__} + {repr(other)}"

        return Get(new_f)

    def __truediv__(self, other: Union[Any, "Get"]):
        return self + '/' + other

    def __repr__(self):
        return f"<{self.__class__.__qualname__} {self._f.__name__} object>"


g = get = Get
