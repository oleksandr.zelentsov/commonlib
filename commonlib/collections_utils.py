import hashlib
import random
import collections
import itertools
from typing import Sequence, Iterable, Mapping, Union, List

from commonlib.constants import K, V


def reference_sorted(
    coll: Sequence, reference: Sequence, unknown_to_front: bool = False
):
    """
    Sort a collection using another collection as a reference.

    If ``unknown_to_front`` is ``True``, the items from ``coll`` that do not exist in
    ``reference``, will be added to the front of the collection, otherwise to the
    beginning.
    """
    if unknown_to_front:
        default_index = float("-inf")
    else:
        default_index = float("inf")

    return sorted(
        coll,
        key=lambda x: (
            reference.index(x) if reference.count(x) != 0 else default_index
        ),
    )


def group_mappings(
    mappings: Iterable[Mapping[K, V]],
    by: Union[List[K], K],
    default_value: Union[List[V], V] = None,
):
    """
    Build a dict with:
    - the values of `by` in every mapping as keys
    - lists of these mappings with such value as values
    """
    is_by_multiple = isinstance(by, list)

    if is_by_multiple:
        if default_value is None:
            default_value = [None] * len(by)
        elif not isinstance(default_value, list):
            raise ValueError(
                "wrong default_value argument, should be list with same length as `by`"
            )

    result = collections.defaultdict(list)
    for mapping in mappings:
        if is_by_multiple:
            selector = tuple(
                [mapping.get(by_1, default_value[i]) for i, by_1 in enumerate(by)]
            )
        else:
            selector = mapping.get(by, default_value)

        result[selector].append(mapping)

    return result


def take(iterable, count=1):
    res = itertools.takewhile(
        lambda v: v[0] < count, ((i, x) for i, x in enumerate(iterable))
    )
    return list((x for i, x in res))


def random_hash_stream(hash_func=hashlib.md5, rand_num_range=(0, 255)):
    for _ in itertools.count():
        yield hash_func(bytes(random.randint(*rand_num_range))).hexdigest()
