from unittest import TestCase

from commonlib import LF
from .settings import TESTED_FUNCTION_SPACE, ALL_WITH_N_TEST_CASES


class LogicalFunctionTestCase(TestCase):
    def test_function_space_number_stays_the_same(self):
        """Test integrity of mapping between numbers and logical functions."""
        tested_range = range(TESTED_FUNCTION_SPACE)
        functions = LF.from_range(TESTED_FUNCTION_SPACE)
        for space_number, func in zip(tested_range, functions):
            with self.subTest(space_number):
                self.assertEqual(
                    space_number,
                    func.function_space_number,
                    "{func} space number should be {should}, is instead {real}".format(
                        func=func, should=space_number, real=func.function_space_number
                    ),
                )

    def test_all_with_n_funcs(self):
        """Check if the amount of generated functions for a certain number of arguments is correct."""
        for argument_count, expected_function_count in ALL_WITH_N_TEST_CASES:
            with self.subTest(
                argument_count=argument_count,
                expected_function_count=expected_function_count,
            ):
                funcs = LF.all_with_n_arguments(argument_count)
                function_count = len(list(funcs))
                self.assertEqual(
                    function_count,
                    expected_function_count,
                    "function count should be {}, is instead {}".format(
                        function_count, expected_function_count
                    ),
                )
